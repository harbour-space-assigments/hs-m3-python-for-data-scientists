import numpy as np
class KNN:
    def __init__(self, k=1, metric=None):
        self.k, self.metric = k, metric
         
    def fit(self, X, y):
        self.train_X, self.train_y = X, Y

    def predict(self, X, num_loops=0):
        if num_loops == 0:
            dists = self.compute_distances_no_loops(X)
        elif num_loops == 1:
            dists = self.compute_distances_one_loop(X)
        elif num_loops == 2:
            dists = self.compute_distances_two_loops(X)
        else:
            raise ValueError('Invalid value %d for num_loops' % num_loops)

        return self.predict_labels(dists)

    def compute_distances_two_loops(self, X):
        num_test = X.shape[0]
        num_train = self.train_X.shape[0]
        dists = np.zeros((num_test, num_train))
        if self.metric == 'manhattan':
            for i_test in range(num_test):
                for i_train in range(num_train):
                    dists[i_test, i_train] = np.sum ( np.abs ( X[i_test] - self.train_X[i_train] ) )
                    pass
        elif self.metric == 'euclidean':
            for i_test in range(num_test):
                for i_train in range(num_train):
                    dists[i_test, i_train] = np.sum ( np.square ( X[i_test] - self.train_X[i_train] ) )
       
        return dists

    def compute_distances_one_loop(self, X):
        num_test = X.shape[0]
        num_train = self.train_X.shape[0]
        dists = np.zeros((num_test, num_train))
        if self.metric == 'manhattan':
            for i in range(num_test):
               dists[i, :] = np.sum(np.abs(X[i] - self.train_X), axis=1) 
        elif self.metric == 'euclidean':
            for i in range(num_test):
                dists[i, :] = np.sum(np.square(X[i] - self.train_X), axis=1) 
        return dists

    def compute_distances_no_loops(self, X):
        num_test = X.shape[0]
        num_train = self.train_X.shape[0]
        dists = np.zeros((num_test, num_train))

        a, b = X, self.train_X
        
        if self.metric == 'manhattan':
            a = a[:,np.newaxis,:]
            b = b[np.newaxis,:,:]
            diff= np.abs(a-b)
            dists = np.sum(diff,axis=2)
            
        elif self.metric == 'euclidean':
            a = a[:,np.newaxis,:]
            b = b[np.newaxis,:,:]
            diff= (a-b)**2
            dists = np.sqrt(np.sum(diff,axis=2))
        return dists

    def predict_labels(self, dists):
        num_test = dists.shape[0]
        y_pred = np.zeros(num_test)
        for i in range(num_test):
            closest_y = []
            ind = np.argsort(dists)[i, ]
            closest_y = self.train_y[ind][:self.kls] #the first K element
            y_pred[i] = np.bincount(closest_y).argmax()
        return y_pred

    def predict(self, X, num_loops=0):
        if num_loops == 0:
            dists = self.compute_distances_no_loops(X)
        elif num_loops == 1:
            dists = self.compute_distances_one_loop(X)
        else:
            dists = self.compute_distances_two_loops(X)

        if self.train_y.dtype == np.bool:
            return self.predict_labels_binary(dists)
        else:
            return self.predict_labels_multiclass(dists)

    def predict_labels_binary(self, dists):
        num_test = dists.shape[0]
        pred = np.zeros(num_test, np.bool)
        for i in range(num_test):
            idx = np.argmin(dists[i,:])
            pred[i] = self.train_y[idx]
            
        return pred

    def predict_labels_multiclass(self, dists):
        num_test = dists.shape[0]
        pred = np.zeros(num_test, np.int)

        for i in range(num_test):
            idx = np.argmin(dists[i,:])
            pred[i] = self.train_y[idx]
        return pred