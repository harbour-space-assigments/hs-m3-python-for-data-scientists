from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score

def binary_classification_metrics(prediction, ground_truth):
    return precision_score(ground_truth, prediction), recall_score(ground_truth, prediction), f1_score(ground_truth, prediction), accuracy_score(ground_truth, prediction)

def multiclass_accuracy(prediction, ground_truth):
    return accuracy_score(ground_truth, prediction)