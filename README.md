## Overview
This course focuses on the fundamentals of Software Engineering. Proper design is an important part of any project. This course covers the basics of the Python programming language, basic concepts and language constructs. Along with this, this course provides tools for using Python programming language in complex projects in the Data Science domain. Students will gain insights into the correct design of the code, maintaining the documentation, codebase and interaction with remote machines. The course introduces students to basic integrations of Python with external libraries: pandas, numpy, matplotlib, sklearn and basic skills of data wrangling.

## Course link
https://harbour.space/data-science/courses/python-for-data-scientists2-ilia-sklonin-432

## License
The Laravel framework is open-sourced software licensed under the [MIT license](LICENSE.md).